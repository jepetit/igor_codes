#pragma rtGlobals=3		// Use modern global access method and strict wave access.

// OSc=2*O:C - H:C

//O:C_AA=3.82*f_44 + 0.0794
//H:C_AA=1.01 + 6.07*f_43 -16.01*f_43�

Function OC_HC(Org_mx,WaveAmus,AikenImproved)

	wave Org_mx,WaveAmus
	string AikenImproved
	
	variable Column29,Column43,Column44
	FindLevel/Q WaveAmus, 29
	Column29=V_LevelX
	FindLevel/Q WaveAmus, 43
	Column43=V_LevelX
	FindLevel/Q WaveAmus, 44
	Column44=V_LevelX
	
	Make/O/N=(dimsize(Org_mx,0)) OM_tot,OC_AA,HC_AA,OSc
	
	variable i,j
	for(i=0;i<dimsize(Org_mx,0)-1;i+=1)
		for(j=0;j<dimsize(Org_mx,1)-1;j+=1)
			OM_tot[i]+=org_mx[i][j]
		endfor
	endfor
	
	OC_AA=3.82*(Org_mx[p][column44]/OM_tot) + 0.0794
	HC_AA=1.01 + 6.07*(Org_mx[p][column43]/OM_tot) - 16.01*(Org_mx[p][column43]/OM_tot)*(Org_mx[p][column43]/OM_tot)
	OSc=2*OC_AA-HC_AA
	
	if (stringmatch(AikenImproved,"Improved"))
		duplicate/O OC_AA,OC_IA
		duplicate/O HC_AA,HC_IA
// O:C_IA=O:C_AA*(1.26-0.623*f_44 + 2.28*f_29)
// H:C_IA=H:C_AA*(1.07+1.07*f_29)
		OC_IA=OC_AA*(1.26-0.623*(Org_mx[p][column44]/OM_tot)+2.28*(Org_mx[p][column29]/OM_tot))
		HC_IA=HC_AA*(1.07+1.07*(Org_mx[p][column44]/OM_tot) )
		OSc=2*OC_IA-HC_IA
	endif

End Function