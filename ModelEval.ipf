#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#include <Percentile and Box Plot>



Function ExtractNonNaN(obs,pred)
	wave obs,pred
	
	duplicate/O obs,bool
	bool=1
	
	duplicate/O obs, obs_temp
	duplicate/O pred,pred_temp
	
	MatrixOP/O obs_temp=replaceNaNs(obs_temp,-999)
	MatrixOP/O pred_temp=replaceNaNs(pred_temp,-999)
	
	bool[]=(obs_temp[p]==-999) ? 0 : bool[p]
	bool[]=(pred_temp[p]==-999) ? 0 : bool[p]
	
	extract/O obs,obs2use,bool==1
	extract/O pred,pred2use,bool==1
	
	killwaves/Z obs_temp,pred_temp,bool
end Function



//Fractionnal Bias
Function FractionnalB(obs,pred)
	wave obs, pred
	
	variable result
	result=(mean(obs)-mean(pred))/(0.5*(mean(obs)+mean(pred)))
	return result

End Function

//Fa2
Function Fa2(obs,pred)
	wave obs,pred
	
	duplicate/O obs, ratio,bool
	ratio=pred/obs
	bool=0
	
	bool[]=(ratio[p]>=0.5 && ratio[p]<=2) ? 1 : bool[p]
	
	variable Fa2_var=sum(bool)/numpnts(obs)
	killwaves/Z bool,ratio
	return Fa2_var
end function


//Mean Bias
Function MeanBias(obs,pred)
	wave obs,pred
	
	duplicate/O obs,diff
	diff=pred-obs
	
	variable MB=sum(diff)/numpnts(diff)
	killwaves/Z diff
	return MB
end function


//Mean Gross Error
Function MeanGrossError(obs,pred)
	wave obs,pred
	
	variable MGE=abs(MeanBias(obs,pred))
	return MGE
end function

//Normalized Mean Bias
Function NormMeanBias(obs,pred)
	wave obs,pred
	
	duplicate/O obs,diff
	diff=pred-obs
	
	variable NMB=sum(diff)/sum(obs)
	killwaves/Z diff
	return NMB
end function


//Normalized Mean Gross Error
Function NormMeanGrossError(obs,pred)
	wave obs,pred
	
	variable NMGE=abs(NormMeanBias(obs,pred))
	return NMGE
end function

//root mean squared error
Function RootMeanSqErr(obs,pred)
	wave obs,pred
	
	duplicate/O obs,sqdiff
	sqdiff=(pred-obs)^2
	
	variable RMSE=sqrt(sum(sqdiff)/numpnts(obs))
	killwaves/Z sqdiff
	return RMSE
end function

//correlation coefficient
Function CorrCoeff(obs,pred)
	wave obs,pred
	
	StatsLinearCorrelationTest/Q obs,pred
	wave W_StatsLinearCorrelationTest
	variable r=W_StatsLinearCorrelationTest[1]
	
	return r
	killwaves/Z W_StatsLinearCorrelationTest
end function

//coefficient of efficiency
Function CoeffEff(obs,pred)
	wave obs,pred
	
	duplicate obs,diff,diff2
	diff=abs(pred-obs)
	diff2=abs(obs-mean(obs))
	
	variable COE=1-(sum(diff)/sum(diff2))
	
	killwaves/Z diff,diff2
	return COE
end function

//Index Agreement
Function IndexAgr(obs,pred,c)
	wave obs,pred
	variable c
	
	duplicate obs,diff,diff2
	diff=abs(pred-obs)
	diff2=abs(obs-mean(obs))
	
	variable IOA
	if (sum(diff)<=(c*sum(diff2)))
		IOA=1-(sum(diff)/(c*sum(diff2)))
	else
		IOA=(sum(diff)/(c*sum(diff2)))-1
	endif
	killwaves/Z diff,diff2
	return IOA
end function


Function ModelEval(obs,pred)
	wave obs,pred
	
	Make/O/N=10 EvalStat
	Make/T/O/N=10 EvalParam
	
	ExtractNonNaN(obs,pred)
	wave obs2use,pred2use
	
	EvalParam[0]="FAC2"
	EvalParam[1]="MB"
	EvalParam[2]="MGE"
	EvalParam[3]="NMB"
	EvalParam[4]="NMGE"
	EvalParam[5]="RMSE"
	EvalParam[6]="r"
	EvalParam[7]="COE"
	EvalParam[8]="IOA"
	EvalParam[9]="FB"
	
	EvalStat[0]=Fa2(obs2use,pred2use)
	EvalStat[1]=MeanBias(obs2use,pred2use)
	EvalStat[2]=MeanGrossError(obs2use,pred2use)
	EvalStat[3]=NormMeanBias(obs2use,pred2use)
	EvalStat[4]=NormMeanGrossError(obs2use,pred2use)
	EvalStat[5]=RootMeanSqErr(obs2use,pred2use)
	EvalStat[6]=CorrCoeff(obs2use,pred2use)
	EvalStat[7]=CoeffEff(obs2use,pred2use)
	EvalStat[8]=IndexAgr(obs2use,pred2use,2)
	EvalStat[9]=FractionnalB(obs2use,pred2use)
	
	edit evalparam,evalstat
	ScatterPlot_ObsPred(obs2use,pred2use)
	
	killwaves/Z obs2use,pred2use
End Function

Function ScatterPlot_ObsPred(obs,pred)
	wave obs, pred
	
	Make/O/N=(11) pred_bin, obs_p10,obs_p25,obs_p50,obs_p75,obs_p90,obs_IQR
	pred_bin=0.1*p*wavemax(pred)
	
	variable i
	for (i=0;i<10;i+=1)
		Extract/O obs,temp,obs>pred_bin[i] && obs<pred_bin[i+1]
		//obs_med[i]=mean(temp)
		fWavePercentile("temp", "10;25;50;75;90", "temp_p", 0, 	0, 0)
		wave tempsort, tempmatrix,tmppercentiles,pcnames,temp_p_N,temp_p_10,temp_p_25,temp_p_50,temp_p_75,temp_p_90
		killwaves tempsort, tempmatrix,tmppercentiles,pcnames,temp_p_N
		obs_p50[i]=temp_p_50[0]
		obs_p10[i]=temp_p_10[0]
		obs_p25[i]=temp_p_25[0]
		obs_p75[i]=temp_p_75[0]
		obs_p90[i]=temp_p_90[0]
		killwaves temp_p_10,temp_p_25,temp_p_50,temp_p_75,temp_p_90
		
	endfor
	
	obs_IQR=obs_p75-obs_p25
	
	Display obs_p50 vs pred_bin
	AppendToGraph pred_bin vs pred_bin
	ModifyGraph lstyle(pred_bin)=7,rgb(pred_bin)=(0,0,0)
	ModifyGraph mode(obs_p50)=4,marker(obs_p50)=19,useMrkStrokeRGB(obs_p50)=1;DelayUpdate
	ErrorBars obs_p50 Y,wave=(obs_IQR,obs_IQR)
	
	
End Function
