#pragma rtGlobals=3		// Use modern global access method and strict wave access.


Function DataUpdate_Task(s)   
	STRUCT WMBackgroundStruct &s
	Triggered_OnlineUpdate()
	UpdateGraphAxis("OnlineGraph")
	
	string timestr=time()
	variable hour, minute
	hour=str2num(stringfromlist(0,timestr,":"))
	minute=str2num(stringfromlist(1,timestr,":"))
	if (hour==0 && minute<30)
	//update CAPS
		//Get Today's date info
		SaveExperiment
		string dayOfMonth, month, year
		year=ExtractDateInfo(datetime-3600*24,"year")
		month=ExtractDateInfo(datetime-3600*24,"month")
		dayOfMonth=ExtractDateInfo(datetime-3600*24,"dayOfMonth")
		string nameoffile="CAPS_"+year+month+dayofmonth+".dat"
		print nameoffile
		//UpdateCAPS(nameoffile)
		
	//Calculate averages for all variables from previous day
	
	endif
	
	return 0
End

Function StartTask()
	Variable numTicks = 18000	//1 tick=1/60 s
	CtrlNamedBackground Test, period=numTicks, proc=DataUpdate_Task
	CtrlNamedBackground Test, start
End


Function StopTask()
	CtrlNamedBackground Test, stop
	
End




Function ImportCAPS()
	
	SetDataFolder root:CAPS
	
	Variable refNum
	String message = "Select one or more files"
	String outputPaths
	String fileFilters = "All Files:.*;"
 
	Open /D /R /MULT=1 /F=fileFilters /M=message refNum
	outputPaths = S_fileName
 
	if (strlen(outputPaths) == 0)
		Print "Cancelled"

	else
		Variable numFilesSelected = ItemsInList(outputPaths, "\r")
		Variable i
		string nameofCAPSfile, nameOfCAPSdate
		for(i=0; i<numFilesSelected; i+=1)
			String path = StringFromList(i, outputPaths, "\r")

			LoadWave/Q/O/J/M/D/K=0/L={31,32,0,0,9}/V={","," $",0,0} path
			wave wave0
			nameofCAPSfile="CAPS_"+num2str(i)
			Rename wave0,$nameofCAPSfile
			
			LoadWave/A=CAPStime_/Q/J/V={","," $",0,0}/L={0,32,0,9,1}/R={French,2,2,2,1,"Year/Month/DayOfMonth",40} path
		endfor
		string ListofCAPSWave=Wavelist("CAPS_*",";","")
		string ListofDateWave=Wavelist("CAPStime_*",";","")
		Concatenate/O/NP=0 ListofDateWave, CAPSdate
		Concatenate/O/NP=0 ListofCAPSWave, Mx
		
	endif
	
	Online_KillListofWaves(ListofCAPSWave)
	Online_KillListofWaves(ListofDateWave)

	RemoveCAPSBlk()
	
End Function



Function RemoveCAPSBlk()

	SetDataFolder root:CAPS:
	wave Mx, CAPSdate
	
	Make/O/N=(dimsize(Mx,0)) Extinction, status
	Extinction=Mx[p][1]
	status=Mx[p][7]
	Extinction[]=(status[p] != 10026) ? NaN : Extinction[p]

	KillWaves/Z status
	
End Function


Function CorrectCAPSTime()
	SetDataFolder root:CAPS:
	wave CAPSdate
	duplicate/O CAPSdate CAPSdate_UTC
	CAPSdate_UTC-=3600

End Function


Function Online_KillListofWaves(ListofWave)
	string ListOfWave
	variable i
	Variable numFilesSelected = ItemsInList(ListOfWave, ";")
	for(i=0; i<numFilesSelected; i+=1)
		String Wave2Kill = StringFromList(i, ListOfWave, ";")
		KillWaves/Z $Wave2Kill	
	endfor
End Function



Function ImportAE33()
	
	SetDataFolder root:AE33
	
	Variable refNum
	String message = "Select one or more files"
	String outputPaths
	String fileFilters = "Data Files (*.raw,*.dat,*.csv):.raw,.dat,.csv;"
 
	Open /D /R /MULT=1 /F=fileFilters /M=message refNum
	outputPaths = S_fileName
 
	if (strlen(outputPaths) == 0)
		Print "Cancelled"
	else
		Variable numFilesSelected = ItemsInList(outputPaths, "\r")
		Variable i
		string nameofAE33file, nameOfAE33date
		for(i=0; i<numFilesSelected; i+=1)
			String path = StringFromList(i, outputPaths, "\r")

			LoadWave/Q/O/J/M/D/K=0/L={0,0,0,3,0}/V={" "," $",0,0} path
			wave wave0
			nameofAE33file="AE33_"+num2str(i)
			Rename wave0,$nameofAE33file
			
			LoadWave/A=AE33time_/Q/J/V={"	"," $",0,0}/L={0,0,0,1,1}/R={French,2,2,2,1,"Year-Month-DayOfMonth",40} path
		endfor
		string ListofAE33Wave=Wavelist("AE33_*",";","")
		string ListofDateWave=Wavelist("AE33time_*",";","")
		Concatenate/O/NP=0 ListofDateWave, AE33date
		Concatenate/O/NP=0 ListofAE33Wave, Mx
		
	endif
	
	Online_KillListofWaves(ListofAE33Wave)
	Online_KillListofWaves(ListofDateWave)

End Function


Function CreateBCWaves()
	
	SetDataFolder root:AE33
	wave Mx
	
	variable rows=dimsize(Mx,0)
	Make/O/N=(rows) BC1, BC2, BC3, BC4, BC5, BC6, BC7
	
	BC1[]=Mx[p][38]
	BC2[]=Mx[p][41]
	BC3[]=Mx[p][44]
	BC4[]=Mx[p][47]
	BC5[]=Mx[p][50]
	BC6[]=Mx[p][53]
	BC7[]=Mx[p][56]
	
	BC1[]=(BC1[p]==0) ? NaN : BC1[p]
	BC2[]=(BC2[p]==0) ? NaN : BC2[p]
	BC3[]=(BC3[p]==0) ? NaN : BC3[p]
	BC4[]=(BC4[p]==0) ? NaN : BC4[p]
	BC5[]=(BC5[p]==0) ? NaN : BC5[p]
	BC6[]=(BC6[p]==0) ? NaN : BC6[p]
	BC7[]=(BC7[p]==0) ? NaN : BC7[p]
	
End Function

Function Absorption()
	SetDataFolder root:AE33
	Wave BC5

	duplicate/O BC5 Abs_BC5
	Abs_BC5*=10.35/1000
	Abs_BC5/=1.76
	
End Function


Function ImportAurora()
	
	SetDataFolder root:Aurora
	
	Variable refNum
	String message = "Select one or more files"
	String outputPaths
	String fileFilters = "Data Files (*.raw,*.dat,*.csv):.raw,.dat,.csv;"
 
	Open /D /R /MULT=1 /F=fileFilters /M=message refNum
	outputPaths = S_fileName
 
	if (strlen(outputPaths) == 0)
		Print "Cancelled"
	else
		Variable numFilesSelected = ItemsInList(outputPaths, "\r")
		Variable i
		string nameofNephfile, nameOfNephdate
		for(i=0; i<numFilesSelected; i+=1)
			String path = StringFromList(i, outputPaths, "\r")

			LoadWave/Q/O/J/M/D/K=0/L={0,0,0,3,0}/V={" "," $",0,0} path
			wave wave0
			nameofNephfile="Aurora_"+num2str(i)
			Rename wave0,$nameofNephfile
			
			LoadWave/A=Auroratime_/Q/J/V={"	"," $",0,0}/L={0,0,0,1,1}/R={French,2,2,2,1,"Year-Month-DayOfMonth",40} path
		endfor
		string ListofNephWave=Wavelist("Aurora_*",";","")
		string ListofDateWave=Wavelist("Auroratime_*",";","")
		Concatenate/O/NP=0 ListofDateWave, Auroradate
		Concatenate/O/NP=0 ListofNephWave, Mx
		
	endif
	
	Online_KillListofWaves(ListofNephWave)
	Online_KillListofWaves(ListofDateWave)

End Function

Function CreateDiffWave()

	SetDataFolder root:Aurora
	wave Mx,AuroraDate
	
	variable rows=dimsize(Mx,0)
	Make/O/N=(rows) Diff1,Diff2,Diff3, status

	Diff1[]=Mx[p][0]
	Diff2[]=Mx[p][1]
	Diff3[]=Mx[p][2]
	
	status=Mx[p][11]
	
	Diff1[]=(status[p]!=7) ? NaN : Diff1[p]
	Diff2[]=(status[p]!=7) ? NaN : Diff2[p]
	Diff3[]=(status[p]!=7) ? NaN : Diff3[p]
	
	Make/O/N=(rows) Onlinehour,Onlineminutes
	variable i
	for(i=0;i<numpnts(Onlineminutes);i+=1)
		Onlinehour[i]=ExtractTimeInfo(AuroraDate[i],"hour")
		Onlineminutes[i]=ExtractTimeInfo(AuroraDate[i],"minute")
	endfor
	
	Diff1[]=(Onlinehour[p]==0 && Onlineminutes[p]<=30) ? NaN : Diff1[p]
	Diff2[]=(Onlinehour[p]==0 && Onlineminutes[p]<=30) ? NaN : Diff2[p]
	Diff3[]=(Onlinehour[p]==0 && Onlineminutes[p]<=30) ? NaN : Diff3[p]
	
	KillWaves/Z Onlinehour, Onlineminutes, status
	
End Function


Function ExtractTimeInfo(dt,timeinfo)
	variable dt
	string timeinfo
	
	variable time
	variable hour, minute, second
	
	if (stringmatch(timeinfo,"hour"))
		time = mod(dt,24*60*60)
		hour=trunc(time/3600)
 		return hour
 	elseif (stringmatch(timeinfo,"minute"))
 		time = mod(dt,3600)
 		minute=trunc(time/60)
 		return minute
 	elseif (stringmatch(timeinfo,"second"))
 		time = mod(dt,60)
 		second=trunc(time)
 		return second
 	endif
	
End Function


Function OnlineCAPSUpload(fileprefix,filedate)
	string fileprefix,filedate


End Function


Function Averages()
	SetDataFolder root:
	wave Timeline1h
	SetDataFolder root:AE33:
	wave AE33date,Abs_BC3
	Conc_avg(Abs_BC3, AE33date,Timeline1h)
	SetDataFolder root:Aurora:
	wave AuroraDate,Diff2
	Conc_avg(Diff2, AuroraDate,Timeline1h)
	SetDataFolder root:CAPS:
	wave CAPSdate_UTC,Extinction
	Conc_avg(Extinction, CAPSdate_UTC,Timeline1h)

End Function


Function Conc_avg(Conc_Wave, Date_Wave,Timeline)
	wave Conc_Wave,Date_Wave,Timeline
	
	duplicate/O Conc_wave temp_conc
	duplicate/O Date_Wave, temp_date
	
	temp_date[]=(numtype(temp_conc[p])==2) ? NaN : temp_date[p]
	
	WaveTransform zapNaNs temp_date
	WaveTransform zapNaNs temp_conc
	
	string ConcWaveName=NameOfWave(Conc_Wave)+"_avg"
	Make/O/N=(numpnts(Timeline)) temp_avg
	variable i,j,k
	for (i=0;i<numpnts(Timeline)-1;i+=1)
		j=BinarySearch(temp_date,Timeline[i])
		k=BinarySearch(temp_date,Timeline[i+1])
		if (j==k)
			temp_avg[i]=nan
			continue
		endif
		temp_avg[i]=mean(temp_conc,j,k-1)
	endfor
	duplicate/O temp_avg $ConcWaveName
	KillWaves temp_avg,temp_conc,temp_date
End Function



Function SumAbsScat()
	
	SetDataFolder root:AE33
	wave Abs_BC3_avg
	SetDataFolder root:Aurora
	wave Diff2_avg
	
	SetDataFolder root:
	duplicate/O Abs_BC3_avg,CalculatedExtinction

	CalculatedExtinction=Abs_BC3_avg+Diff2_avg

End Function


Function CalculateAbsorption()
	
	SetDataFolder root:Aurora
	wave Diff2_avg
	SetDataFolder root:CAPS
	wave Extinction_avg
	
	SetDataFolder root:
	duplicate/O Diff2_avg,CalculatedAbsorption

	CalculatedAbsorption=Extinction_avg-Diff2_avg

End Function


Function ImportACSM()
	
	SetDataFolder root:ACSM
	
	Variable refNum
	String message = "Select one or more files"
	String outputPaths
	String fileFilters = "All Files:.*;"
 
	Open /D /R /MULT=1 /F=fileFilters /M=message refNum
	outputPaths = S_fileName
 
	if (strlen(outputPaths) == 0)
		Print "Cancelled"

	else
		Variable numFilesSelected = ItemsInList(outputPaths, "\r")
		Variable i
		string nameofACSMfile, nameOfACSMdate
		for(i=0; i<numFilesSelected; i+=1)
			String path = StringFromList(i, outputPaths, "\r")

			LoadWave/Q/O/J/M/D/K=0/L={0,1,1,1,5}/V={"	"," $",0,0} path
			wave wave0
			nameofACSMfile="ACSM_"+num2str(i)
			Rename wave0,$nameofACSMfile
			
			LoadWave/A=ACSMtime_/Q/J/V={","," $",0,0}/L={0,1,1,0,1}/R={French,2,2,2,1,"Year/Month/DayOfMonth",40} path
		endfor
		string ListofACSMWave=Wavelist("ACSM_*",";","")
		string ListofDateWave=Wavelist("ACSMtime_*",";","")
		Concatenate/O/NP=0 ListofDateWave, ACSMdate
		Concatenate/O/NP=0 ListofACSMWave, Mx
		
	endif
	
	Online_KillListofWaves(ListofACSMWave)
	Online_KillListofWaves(ListofDateWave)

	Make/O/N=(dimsize(Mx,0)) OM, NO3, SO4, NH4,Cl
	OM[]=Mx[p][0]
	NO3[]=Mx[p][1]
	SO4[]=Mx[p][2]
	NH4[]=Mx[p][3]
	Cl[]=Mx[p][4]
	
End Function



Function ONline_CalcCE_dry(NH4_DL,CE_lowNH4)
	Variable NH4_DL,CE_lowNH4
	
	//  Prior to running this procedure, all species must be calculated using the same CE.
	//  The sampling line relative humidity (if measured) should be named "RH_SL"
	
	SetDataFolder root:ACSM
	
	wave SO4, NH4, NO3, Cl, OM
	
	if (numpnts(OM)==0)
		duplicate/o OM CE_dry
		duplicate/o SO4 SO4_CEcorr
		duplicate/o NH4 NH4_CEcorr
		duplicate/o NO3 NO3_CEcorr
		duplicate/o Cl Cl_CEcorr
		duplicate/o OM OM_CEcorr
	else
		//	Create waves of each species to smooth for the calculations.
		duplicate/o SO4 SO4_CE1
		duplicate/o NH4 NH4_CE1
		duplicate/o NO3 NO3_CE1
		duplicate/o Cl Cl_CE1
		duplicate/o OM OM_CE1
		
		Variable i
		Duplicate/o SO4 PredNH4_CE1, NH4_MeasToPredict, ANMF
		Duplicate/o SO4 CE_dry
		CE_dry=0.5
	
		//	Equation 3
		PredNH4_CE1=18*(SO4_CE1/96*2+NO3_CE1/62+Cl_CE1/35.45)
		NH4_MeasToPredict=NH4_CE1/PredNH4_CE1
		
		//	Equation 5
		ANMF=(80/62)*NO3_CE1/(NO3_CE1+SO4_CE1+NH4_CE1+OM_CE1+Cl_CE1)
	
		//	Calculate the dry collection efficiency, CE_dry
		For (i=0;i<(numpnts(SO4_CE1));i+=1)
			//	Nan negative NH4_MeasToPredict points
			If (NH4_MeasToPredict[i]<0)
				NH4_MeasToPredict[i]=nan
			EndIf
	
			//	Nan ANMF points if negative or more than 1
			If (ANMF[i]<0)
				ANMF[i]=nan
			ElseIf (ANMF[i]>1)
				ANMF[i]=nan
			EndIf
			
			If (PredNH4_CE1[i]<NH4_DL)
				CE_dry[i]=CE_lowNH4
			ElseIf (NH4_MeasToPredict[i]>=0.75)
				//	Apply Equation 4
				CE_dry[i]= 0.0833+0.9167*ANMF[i]
			ElseIf (NH4_MeasToPredict[i]<0.75)
				//	Apply Equation 6
				CE_dry[i]= 1-0.73*NH4_MeasToPredict[i]
			EndIf
		EndFor
		
		//	Make CE_dry between 0.45 and 1
		CE_dry=min(1,(max(0.5,CE_dry)))
		
		KillWaves ANMF,SO4_CE1,NH4_CE1,NO3_CE1,Cl_CE1, OM_CE1, PredNH4_CE1, NH4_MeasToPredict 
		
		duplicate/o SO4 SO4_CEcorr
		duplicate/o NH4 NH4_CEcorr
		duplicate/o NO3 NO3_CEcorr
		duplicate/o Cl Cl_CEcorr
		duplicate/o OM OM_CEcorr
		
		SO4_CEcorr=SO4*0.5/CE_dry
		NH4_CEcorr=NH4*0.5/CE_dry
		NO3_CEcorr=NO3*0.5/CE_dry
		Cl_CEcorr=Cl*0.5/CE_dry
		OM_CEcorr=OM*0.5/CE_dry
		
	
	endif
	
	
End Function




Function Triggered_OnlineUpdate()
	
	//Get Today's date info
	string dayOfMonth, month, year
	year=ExtractDateInfo(datetime,"year")
	month=ExtractDateInfo(datetime,"month")
	dayOfMonth=ExtractDateInfo(datetime,"dayOfMonth")
	print secs2time(datetime,0)
	
	
	//check if updated AE33 file
	string path_AE33_str="C:Users:dpeti:Documents:ebas_aerosol_nrt:data:ae33_sn53:"
	string file_prefix="ae33_sn53__"
	string filename=file_prefix+year+month+dayofmonth+"T000000.raw"
	NEwPath/O/Q path_AE33, path_AE33_str
	GetFileFolderInfo/Z/Q/P=path_AE33 filename
	
	if (V_flag != 0)
		print "no AE33 file today..."
		//no update
	else 
		//update
		print "updating AE33..."
		print filename
		UpdateAE33(path_AE33_str,filename)
	endif
	
	
	//check if updated Neph file
	string path_Neph_str="C:Users:dpeti:Documents:ebas_aerosol_nrt:data:aurora3000_sn182409:"
	file_prefix="aurora3000_sn182409__"
	filename=file_prefix+year+month+dayofmonth+"T000000.raw"
	NEwPath/O/Q path_neph, path_neph_str
	GetFileFolderInfo/Z/Q/P=path_neph filename
	
	if (V_flag != 0)
		print "no neph file today..."
		//no update
	else 
		//update
		print "updating neph..."
		print filename
		UpdateNeph(path_neph_str,filename)
	endif
	
	//update Neph
	
	
	//traitement Neph
	
	
	
	//check if updated CAPS file
	
	
	//update CAPS
	
	
	
	//check if new ACSM file
	string path_ACSM_str="C:Users:dpeti:ownCloud:ACMCC_PMF_Export:ACSM:"
	NewPath/O/Q path_ACSM, path_ACSM_str
	string listoffiles=indexedfile(path_acsm,-1,".txt")
	
	
	wave ACSMdate=root:ACSM:ACSMdate
	
	if(numpnts(ACSMdate)<ItemsInList(listoffiles))
		//update
		filename=StringFromList(ItemsInList(listoffiles)-1,listoffiles)
		UpdateACSM(path_ACSM_str,filename)
		
	else
		//no update
		print "no ACSM file yet..."
	endif

	SetDataFolder root:

End Function


Function UpdateACSM(folder,filename)
	string folder, filename
	SetDataFolder root:ACSM:
	wave ACSMdate, Mx

	print "updating ACSM"
	print filename
	
	LoadWave/Q/O/J/M/D/K=0/L={0,1,1,1,5}/V={"	"," $",0,0} (folder+filename)
	wave wave0			
	LoadWave/A=ACSMtime_/Q/J/V={","," $",0,0}/L={0,1,1,0,1}/R={French,2,2,2,1,"Year/Month/DayOfMonth",40} (folder+filename)
	wave ACSMtime_0

	Concatenate/O/NP=0 {ACSMdate, ACSMtime_0}, test
	Concatenate/O/NP=0 {Mx, wave0}, test1
	
	duplicate/O test ACSMdate
	duplicate/O test1 Mx
	
	KillWaves/Z wave0,ACSMtime_0,test,test1

	Make/O/N=(dimsize(Mx,0)) OM, NO3, SO4, NH4,Cl
	OM[]=Mx[p][0]
	NO3[]=Mx[p][1]
	SO4[]=Mx[p][2]
	NH4[]=Mx[p][3]
	Cl[]=Mx[p][4]
	
	CalcCE_dry(0.5,0.5)
	
	wave BC7=root:AE33:BC7
	wave AE33date=root:AE33:AE33date
	Conc_avg(BC7, AE33date,ACSMdate)
	wave BC7_avg
	BC7_avg/=1000
	
	CalculateDiffFromACSM()
	
End Function



Function UpdateAE33(folder,filename)
	string folder, filename
	SetDataFolder root:AE33:
	wave AE33date, Mx
	
	//print "updating AE33..."
	
	variable lastrow=numpnts(AE33date)-1
	LoadWave/A=AE33time/Q/J/V={"	"," $",0,0}/L={0,0,0,1,1}/R={French,2,2,2,1,"Year-Month-DayOfMonth",40} (folder+filename)
	wave AE33time0
	variable lastrowUpdate=numpnts(AE33time0)-1
	variable j, firstline
	
	j=binarysearch(AE33time0,AE33date[lastrow])
	if (j==-1)
		firstline=0
	else
		firstline=j+2
	endif
	//print (folder+filename)
	//print firstline
	KillWaves/Z AE33time0
	
	LoadWave/Q/O/J/M/D/K=0/L={0,firstline,0,3,0}/V={" "," $",0,0} (folder+filename)
	wave wave0
			
	LoadWave/A=AE33time_/Q/J/V={"	"," $",0,0}/L={0,firstline,0,1,1}/R={French,2,2,2,1,"Year-Month-DayOfMonth",40} (folder+filename)
	wave AE33time_0
	
	Concatenate/O/NP=0 {AE33date, AE33time_0}, test
	Concatenate/O/NP=0 {Mx, wave0}, test1
	
	duplicate/O test AE33date
	duplicate/O test1 Mx
	
	KillWaves/Z wave0,AE33time_0,test,test1
	
	CreateBCWaves()
	Absorption()
	
End Function


Function UpdateNeph(folder,filename)
	string folder, filename
	SetDataFolder root:Aurora:
	wave Auroradate, Mx
	
	
	variable lastrow=numpnts(Auroradate)-1
	LoadWave/A=Nephtime/Q/J/V={"	"," $",0,0}/L={0,0,0,1,1}/R={French,2,2,2,1,"Year-Month-DayOfMonth",40} (folder+filename)
	wave Nephtime0
	variable lastrowUpdate=numpnts(Nephtime0)-1
	variable j, firstline
	
	j=binarysearch(Nephtime0,Auroradate[lastrow])
	if (j==-1)
		firstline=0
	else
		firstline=j+2
	endif
	//print (folder+filename)
	//print firstline
	KillWaves/Z Nephtime0
	
	LoadWave/Q/O/J/M/D/K=0/L={0,firstline,0,3,0}/V={" "," $",0,0} (folder+filename)
	wave wave0
			
	LoadWave/A=Nephtime_/Q/J/V={"	"," $",0,0}/L={0,firstline,0,1,1}/R={French,2,2,2,1,"Year-Month-DayOfMonth",40} (folder+filename)
	wave Nephtime_0
	
	
	Concatenate/O/NP=0 {Auroradate, Nephtime_0}, test
	Concatenate/O/NP=0 {Mx, wave0}, test1
	
	duplicate/O test Auroradate
	duplicate/O test1 Mx
	
	KillWaves/Z wave0,Nephtime_0,test,test1
	
	CreateDiffWave()

End Function


Function UpdateCAPS(nameoffile)
	string nameoffile
	SetDataFolder root:CAPS:
	wave CAPSdate, Mx
	
	string folderpath="C:Users:dpeti:OnwCloud:CAPS:"
	
	LoadWave/Q/O/J/M/D/K=0/L={31,32,0,0,9}/V={","," $",0,0} (folderpath+nameoffile)
	wave wave0
		
	LoadWave/A=CAPStime_/Q/J/V={","," $",0,0}/L={0,32,0,9,1}/R={French,2,2,2,1,"Year/Month/DayOfMonth",40} (folderpath+nameoffile)
	wave CAPStime_0
	
	Concatenate/O/NP=0 {CAPStime_0,CAPSdate}, test
	Concatenate/O/NP=0 {Mx,wave0}, test1
	
	duplicate/O test CAPSdate
	duplicate/O test1 Mx
	
	RemoveCAPSBlk()
	CorrectCAPSTime()
	
End Function


Function Makegraph()

	SetDataFolder root:AE33
	wave AE33date,BC1,BC2,BC3,BC4,BC5,BC6,BC7
	
	Display/N=OnlineGraph/L=L1 BC7,BC6,BC5,BC4,BC3,BC2,BC1 vs AE33date
	ModifyGraph/W=OnlineGraph rgb(BC1)=(36864,14592,58880)
	ModifyGraph/W=OnlineGraph rgb(BC2)=(0,43520,65280)
	ModifyGraph/W=OnlineGraph rgb(BC3)=(0,65280,0)
	ModifyGraph/W=OnlineGraph rgb(BC4)=(58880,58880,0)
	ModifyGraph/W=OnlineGraph rgb(BC5)=(65280,0,0)
	ModifyGraph/W=OnlineGraph rgb(BC6)=(39168,13056,0)
	ModifyGraph/W=OnlineGraph rgb(BC7)=(0,0,0)
	Label bottom " "
	ModifyGraph zero(L1)=8
	ModifyGraph axisEnab(L1)={0.67,1}
	ModifyGraph freePos(L1)={0,bottom}
	
	SetDataFolder root:ACSM
	wave ACSMdate,OM_CEcorr,NO3_CEcorr,SO4_CEcorr,NH4_CEcorr,Cl_CEcorr
	AppendToGraph/L=L2 OM_CEcorr,NO3_CEcorr,SO4_CEcorr,NH4_CEcorr,Cl_CEcorr vs ACSMdate
	ModifyGraph zero(L2)=8
	ModifyGraph axisEnab(L2)={0.34,0.65},freePos(L2)={0,bottom}
	ModifyGraph mode(OM_CEcorr)=7,hbFill(OM_CEcorr)=2,toMode(OM_CEcorr)=2
	ModifyGraph mode(NO3_CEcorr)=7,hbFill(NO3_CEcorr)=2,toMode(NO3_CEcorr)=2
	ModifyGraph mode(SO4_CEcorr)=7,hbFill(SO4_CEcorr)=2,toMode(SO4_CEcorr)=2
	ModifyGraph mode(NH4_CEcorr)=7,hbFill(NH4_CEcorr)=2,toMode(NH4_CEcorr)=2
	ModifyGraph mode(Cl_CEcorr)=7,hbFill(Cl_CEcorr)=2,toMode(Cl_CEcorr)=2
	ModifyGraph rgb(OM_CEcorr)=(26112,52224,0),rgb(NO3_CEcorr)=(0,34816,52224)
	ModifyGraph rgb(SO4_CEcorr)=(52224,0,0),rgb(NH4_CEcorr)=(65280,49152,16384)
	ModifyGraph rgb(Cl_CEcorr)=(65280,32768,58880)
	
	SetDataFolder root:Aurora:
	wave Auroradate,diff3,diff2,diff1
	AppendToGraph/L=L3 diff1,diff2,diff3 vs Auroradate
	ModifyGraph zero(L3)=8
	ModifyGraph axisEnab(L3)={0,0.32},freePos(L3)={0,bottom}
	ModifyGraph rgb(Diff1)=(65280,16384,16384)
	ModifyGraph rgb(Diff2)=(0,52224,0)
	ModifyGraph rgb(Diff3)=(0,34816,52224)
	
//	SetDataFolder root:CAPS:
//	wave CAPSdate_UTC,Extinction
//	AppendToGraph/L=L4 Extinction vs CAPSdate_UTC
//	ModifyGraph zero(L4)=8
//	ModifyGraph axisEnab(L4)={0,0.24},freePos(L4)={0,bottom}
//	ModifyGraph rgb(Extinction)=(0,0,0)
	
	ModifyGraph fSize=18
	ModifyGraph grid(bottom)=2,nticks(bottom)=15
	ModifyGraph dateInfo(bottom)={0,0,-1},dateFormat(bottom)={Default,2,2,2,2,"DayOfMonth/Month",-6}
	ModifyGraph gridRGB(bottom)=(52224,52224,52224)
	
	
End Function



Function UpdateGraphAxis(GraphName)
	string GraphName
	GetAxis/Q/W=$GraphName bottom
	//print V_max
	
	variable nbjours=5
	
	variable Bottommin2use=V_max-nbjours*3600*24
	SetAxis/W=$GraphName bottom Bottommin2use,*
	
	variable L1max, L2max, L3max, L4max
	variable BC1max
	wave BC1=root:AE33:BC1

	BC1max=wavemax(BC1,numpnts(BC1)-1-60*24*nbjours,numpnts(BC1)-1)
	
	L1max=BC1max
	
	SetAxis/W=$GraphName L1 *,L1max
	
	wave Diff3=root:Aurora:Diff3
	L3max=wavemax(Diff3,numpnts(Diff3)-1-60*24*nbjours,numpnts(Diff3)-1)
	SetAxis/W=$GraphName L3 *,L3max
	
	SetDataFolder root:ACSM:
	wave OM_CEcorr,NO3_CEcorr,SO4_CEcorr,NH4_CEcorr,Cl_CEcorr
	duplicate/O OM_CEcorr NR_PM1
	
	NR_PM1=OM_CEcorr+NO3_CEcorr+SO4_CEcorr+NH4_CEcorr+Cl_CEcorr
	
	L2max=wavemax(NR_PM1,numpnts(NR_PM1)-1-2*24*nbjours,numpnts(NR_PM1)-1)
	SetAxis/W=$GraphName L2 *,L2max
	
	wave Extinction=root:CAPS:Extinction
	L4max=wavemax(Extinction,numpnts(Extinction)-1-60*24*nbjours,numpnts(Extinction)-1)
	SetAxis/W=$GraphName L4 *,L4max
	
	
End Function



Function/S ExtractDateInfo(dt,dateinfo)
	variable dt					// Input date/time value
 	string dateinfo
 	
	String shortDateStr = Secs2Date(dt, -1)		// <day-of-month>/<month>/<year> (<day of week>)
 
	Variable dayOfMonth, month, year, dayOfWeek
	sscanf shortDateStr, "%d/%d/%d (%d)", dayOfMonth, month, year, dayOfWeek
 	
 	string year_txt, month_txt, dayOfMonth_txt
 	
 	if (stringmatch(dateinfo,"year"))
 		year_txt=num2str(year)
 		return year_txt
 	elseif (stringmatch(dateinfo,"month"))
 		if (month<10)
 			month_txt="0"+num2str(month)
 		else
 			month_txt=num2str(month)
 		endif
 		return month_txt
 	elseif (stringmatch(dateinfo,"dayOfMonth"))
 		if (dayOfMonth < 10)
 			dayOfMonth_txt="0"+num2str(dayOfMonth)
 		else
 			dayOfMonth_txt=num2str(dayOfMonth)
 		endif
 		return dayOfMonth_txt
 	endif
End



Function GraphExtinction()

	SetDataFolder root:CAPS
	wave Extinction, CAPSdate_UTC
	
	Display/N=ExinctionGraph Extinction vs CAPSdate_UTC

	ModifyGraph zero(left)=8
	ModifyGraph rgb(Extinction)=(0,0,0)
	
	SetDataFolder root:Aurora:
	wave Auroradate,diff3,diff2,diff1
	AppendToGraph diff2 vs Auroradate
	ModifyGraph rgb(Diff2)=(0,52224,0)
	ModifyGraph lsize(Diff2)=1.5
	
	SetDataFolder root:AE33:
	wave AE33date,Abs_BC3
	AppendToGraph Abs_BC3 vs AE33date
	ModifyGraph rgb(Abs_BC3)=(47872,47872,47872)
	
	ModifyGraph fSize=18
	Label bottom " "
	Legend/C/N=text0/J/A=MC "\\Z18\\s(Extinction) CAPS\r\\s(Diff2) Neph\r\\s(Abs_BC3) AE33"
	

End Function


Function ONline_Sandradewi(L_UV,L_IR,BC_UV,BC_IR,BC,a_wb,a_ff)
	wave BC_UV, BC_IR, BC
	variable a_wb, a_ff,L_UV,l_IR
	
	variable LO_ratio=L_UV/L_IR
	
	variable LO_ratio_wb=LO_ratio^a_wb
	variable LO_ratio_ff=LO_ratio^(-a_ff)
	
	Make/O/N=(numpnts(BC)) babs_UV,babs_IR,babs_wb_UV,babs_wb_IR,BB,BCwb,BCff
	babs_UV=BC_UV*14.54
	babs_IR=BC_IR*7.77
	
	babs_wb_UV=(babs_UV-LO_ratio_ff*babs_IR)/(1-LO_ratio_wb*LO_ratio_ff)
	babs_wb_IR=babs_wb_UV*LO_ratio_wb
	
	//BB=min(max(0,babs_wb_IR/babs_IR),1)
	BB=babs_wb_IR/babs_IR
	
	BCwb=BB*BC
	BCff=BC-BCwb
	
	//BCwb[] = (BB[p]==1) ? NaN : BCwb[p]
	//BCff[] = (BB[p]==1) ? NaN : BCff[p]
	

	KillWaves babs_UV,babs_IR,babs_wb_UV,babs_wb_IR, BB

End Function


Function ACSM_Contrib()

	SetDataFolder root:ACSM:
	wave OM_CEcorr,NO3_CEcorr,SO4_CEcorr,NH4_CEcorr,Cl_CEcorr, BC7_avg
	duplicate/O OM_CEcorr PM1
	PM1=OM_CEcorr+NO3_CEcorr+SO4_CEcorr+NH4_CEcorr+Cl_CEcorr+BC7_avg
	
	duplicate/O OM_CEcorr fOM,fNO3,fSO4,fNH4,fCl,fBC
	
	fOM=OM_CEcorr/PM1
	fNO3=NO3_CEcorr/PM1
	fSO4=SO4_CEcorr/PM1
	fNH4=NH4_CEcorr/PM1
	fCl=Cl_CEcorr/PM1
	fBC=BC7_avg/PM1
	

End Function

Function CalculateDiffFromACSM()
	SetDataFolder root:ACSM:
	wave OM_CEcorr,NO3_CEcorr,SO4_CEcorr,NH4_CEcorr
	
	duplicate/O OM_CEcorr EstimatedDiff_525
	
	EstimatedDiff_525=5*1*(NO3_CEcorr+SO4_CEcorr+NH4_CEcorr)+4*OM_CEcorr
End Function


Function ImportPMF()
	
	NewDataFolder/O/S root:PMF
	
	Variable refNum
	String message = "Select one or more files"
	String outputPaths
	String fileFilters = "All Files:.*;"
 
	Open /D /R /MULT=1 /F=fileFilters /M=message refNum
	outputPaths = S_fileName
 
	if (strlen(outputPaths) == 0)
		Print "Cancelled"

	else
		Variable numFilesSelected = ItemsInList(outputPaths, "\r")
		Variable i
		string nameofACSMfile, nameOfACSMdate
		for(i=0; i<numFilesSelected; i+=1)
			String path = StringFromList(i, outputPaths, "\r")

			LoadWave/Q/O/J/M/D/K=0/L={0,1,1,1,5}/V={"	"," $",0,0} path
			wave wave0
			nameofACSMfile="ACSM_"+num2str(i)
			Rename wave0,$nameofACSMfile
			
			LoadWave/A=ACSMtime_/Q/J/V={","," $",0,0}/L={0,1,1,0,1}/R={French,2,2,2,1,"Year/Month/DayOfMonth",40} path
		endfor
		string ListofACSMWave=Wavelist("ACSM_*",";","")
		string ListofDateWave=Wavelist("ACSMtime_*",";","")
		Concatenate/O/NP=0 ListofDateWave, ACSMdate
		Concatenate/O/NP=0 ListofACSMWave, Mx
		
	endif
	
	Online_KillListofWaves(ListofACSMWave)
	Online_KillListofWaves(ListofDateWave)

	Make/O/N=(dimsize(Mx,0)) HOA, BBOA, OOA
	HOA[]=Mx[p][0]
	BBOA[]=Mx[p][1]
	OOA[]=Mx[p][2]
	
End Function



Function ImportACSMfromDates(wavedate, pathNextCloud,fileprefix)
	string pathNextCloud, fileprefix
	wave wavedate
	
	
	//SetDataFolder root:ACSM
	
	string startyear=Online_ExtractDateInfo(wavedate[0],"year")
	string startmonth=Online_ExtractDateInfo(wavedate[0],"month")
	string startdayOfMonth=Online_ExtractDateInfo(wavedate[0],"dayOfMonth")
	string starthour=Online_ExtractTimeInfo(wavedate[0],"hour")
	string startminute=Online_ExtractTimeInfo(wavedate[0],"minute")
	string startsecond=Online_ExtractTimeInfo(wavedate[0],"second")
	
	string endyear=Online_ExtractDateInfo(wavedate[0],"year")
	string endmonth=Online_ExtractDateInfo(wavedate[0],"month")
	string enddayOfMonth=Online_ExtractDateInfo(wavedate[0],"dayOfMonth")
	string endhour=Online_ExtractTimeInfo(wavedate[0],"hour")
	string endminute=Online_ExtractTimeInfo(wavedate[0],"minute")
	string endsecond=Online_ExtractTimeInfo(wavedate[0],"second")
	
	string startpath=pathNextCloud+startyear+"\\"+startmonth+"\\"+startdayOfMonth+"\\"
	
	Make/O/D/N=1 datefollowup
	datefollowup=date2secs(str2num(startyear),str2num(startmonth),str2num(startdayOfMonth))
	
	string currentyear, currentmonth,currentday, currentpath, list, filelist,prefix
	string nameofACSMfile, nameOfACSMdate
	
	
	variable i,j,k
	k=0
	for(i=0;datefollowup[0]<= wavedate[1];i+=1)
		currentyear=Online_ExtractDateInfo(datefollowup[0],"year")
		currentmonth=Online_ExtractDateInfo(datefollowup[0],"month")
		currentday=Online_ExtractDateInfo(datefollowup[0],"dayOfMonth")
		currentpath=pathNextCloud+currentyear+"\\"+currentmonth+"\\"+currentday+"\\"
		GetFileFolderInfo/Z/Q currentpath
		if (V_flag==0)
			NewPath/O/Q CurrentPath_Path, CurrentPath
			prefix=fileprefix+currentyear+"*"
		
			list = IndexedFile(CurrentPath_Path, -1, ".txt")
			fileList = ListMatch(list, prefix, ";")
		
			//print filelist
			datefollowup[0]+=86400
		
			Variable numFilesSelected = ItemsInList(fileList, ";")
			for(j=0; j<numFilesSelected; j+=1)
				String filename = StringFromList(j, filelist, ";")
				variable nbcol=50
				LoadWave/Q/O/J/M/D/K=0/L={0,1,1,1,nbcol}/V={"	"," $",0,0} currentpath+filename
				wave wave0
				if (dimsize(wave0,1)!=nbcol)
					k+=1
					Killwaves/Z wave0
					continue
				endif
				
				nameofACSMfile="ACSM_"+num2str(k)
				Rename wave0,$nameofACSMfile
				k+=1
				
				LoadWave/A=ACSMtime_/Q/J/V={","," $",0,0}/L={0,1,1,0,1}/R={French,2,2,2,1,"Year/Month/DayOfMonth",40} currentpath+filename
			endfor
		else
			datefollowup[0]+=86400
			print "folder not found"
		endif
	endfor
	
	string ListofACSMWave=Wavelist("ACSM_*",";","")
	string ListofDateWave=Wavelist("ACSMtime_*",";","")
	Concatenate/O/NP=0 ListofDateWave, ACSMdate
	Concatenate/O/NP=0 ListofACSMWave, Mx
	Online_KillListofWaves(ListofACSMWave)
	Online_KillListofWaves(ListofDateWave)
	KillWaves/Z datefollowup
	Make/O/N=(dimsize(Mx,0)) OM, NO3, SO4, NH4,Cl
	OM[]=Mx[p][0]
	NO3[]=Mx[p][1]
	SO4[]=Mx[p][2]
	NH4[]=Mx[p][3]
	Cl[]=Mx[p][4]
	
	
	Display OM vs ACSMdate
	Appendtograph NO3,SO4,NH4,Cl vs ACSMdate
	ModifyGraph mode(OM)=7,hbFill(OM)=2,toMode(OM)=2
	ModifyGraph mode(NO3)=7,hbFill(NO3)=2,toMode(NO3)=2
	ModifyGraph mode(SO4)=7,hbFill(SO4)=2,toMode(SO4)=2
	ModifyGraph mode(NH4)=7,hbFill(NH4)=2,toMode(NH4)=2
	ModifyGraph mode(Cl)=7,hbFill(Cl)=2,toMode(Cl)=2
	ModifyGraph rgb(OM)=(26112,52224,0),rgb(NO3)=(0,34816,52224)
	ModifyGraph rgb(SO4)=(52224,0,0),rgb(NH4)=(65280,49152,16384)
	ModifyGraph rgb(Cl)=(65280,32768,58880)
	
	
	
	
//	Variable refNum
//	String message = "Select one or more files"
//	String outputPaths
//	String fileFilters = "All Files:.*;"
// 
//	Open /D /R /MULT=1 /F=fileFilters /M=message refNum
//	outputPaths = S_fileName
// 
//	if (strlen(outputPaths) == 0)
//		Print "Cancelled"
//
//	else
//		Variable numFilesSelected = ItemsInList(outputPaths, "\r")
//		Variable i
//		string nameofACSMfile, nameOfACSMdate
//		for(i=0; i<numFilesSelected; i+=1)
//			String path = StringFromList(i, outputPaths, "\r")
//
//			LoadWave/Q/O/J/M/D/K=0/L={0,1,1,1,5}/V={"	"," $",0,0} path
//			wave wave0
//			nameofACSMfile="ACSM_"+num2str(i)
//			Rename wave0,$nameofACSMfile
//			
//			LoadWave/A=ACSMtime_/Q/J/V={","," $",0,0}/L={0,1,1,0,1}/R={French,2,2,2,1,"Year/Month/DayOfMonth",40} path
//		endfor
//		string ListofACSMWave=Wavelist("ACSM_*",";","")
//		string ListofDateWave=Wavelist("ACSMtime_*",";","")
//		Concatenate/O/NP=0 ListofDateWave, ACSMdate
//		Concatenate/O/NP=0 ListofACSMWave, Mx
//		
//	endif
//	
//	Online_KillListofWaves(ListofACSMWave)
//	Online_KillListofWaves(ListofDateWave)
//
//	Make/O/N=(dimsize(Mx,0)) OM, NO3, SO4, NH4,Cl
//	OM[]=Mx[p][0]
//	NO3[]=Mx[p][1]
//	SO4[]=Mx[p][2]
//	NH4[]=Mx[p][3]
//	Cl[]=Mx[p][4]
	
End Function


Function/S Online_ExtractDateInfo(dt,dateinfo)
	variable dt					// Input date/time value
 	string dateinfo
 	
	String shortDateStr = Secs2Date(dt, -1)		// <day-of-month>/<month>/<year> (<day of week>)
 
	Variable dayOfMonth, month, year, dayOfWeek
	sscanf shortDateStr, "%d/%d/%d (%d)", dayOfMonth, month, year, dayOfWeek
 	
 	string year_txt, month_txt, dayOfMonth_txt
 	
 	if (stringmatch(dateinfo,"year"))
 		year_txt=num2str(year)
 		return year_txt
 	elseif (stringmatch(dateinfo,"month"))
 		if (month<10)
 			month_txt="0"+num2str(month)
 		else
 			month_txt=num2str(month)
 		endif
 		return month_txt
 	elseif (stringmatch(dateinfo,"dayOfMonth"))
 		if (dayOfMonth < 10)
 			dayOfMonth_txt="0"+num2str(dayOfMonth)
 		else
 			dayOfMonth_txt=num2str(dayOfMonth)
 		endif
 		return dayOfMonth_txt
 	endif
End

Function/S Online_ExtractTimeInfo(dt,timeinfo)
	variable dt
	string timeinfo
	
	variable time
	string hour, minute, second
	
	if (stringmatch(timeinfo,"hour"))
		time = mod(dt,24*60*60)
		if (trunc(time/3600) < 10)
			hour="0"+num2str(trunc(time/3600))
		else
			hour=num2str(trunc(time/3600))
		endif
 		return hour
 	elseif (stringmatch(timeinfo,"minute"))
 		time = mod(dt,3600)
 		if (trunc(time/60) < 10)
 			minute="0"+num2str(trunc(time/60))
 		else
 			minute=num2str(trunc(time/60))
 		endif
 		return minute
 	elseif (stringmatch(timeinfo,"second"))
 		time = mod(dt,60)
 		if (trunc(time) < 10)
 			second="0"+num2str(trunc(time))
 		else
 			second=num2str(trunc(time))
 		endif
 		return second
 	endif
	
End Function