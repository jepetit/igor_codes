#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#include <Percentile and Box Plot>


Menu "AE33_validation"
	"Launch Panel", Build_AE33Panel()

End


Function Build_AE33Panel()
	NewPanel/N=AE33Panel/W=(0,0,400,400)/K=1
	Button LoadAE33_butt proc=LoadAE33DataFiles, title="Load AE33 data",win=AE33Panel, size={190,30},pos={100,50}, fSize=18, fStyle=1
	Button PlotFlow_butt proc=PlotFlows, title="Plot Flows",win=AE33Panel, size={190,30},pos={100,100}, fSize=18, fStyle=1
	Button SSC_butt proc=ScanSpotChange, title="Scan Spot Change",win=AE33Panel, size={190,30},pos={100,175}, fSize=18, fStyle=1
	
	Button SetValidation_butt proc=SetValidationPanel, title="Set Validation",win=AE33Panel, size={190,30},pos={100,250}, fSize=18, fStyle=1
	
	NewDataFolder/O/S root:AE33data
	
	Variable/G Flow1=1.5
	Variable/G Flow2=3.5
	Variable/G TotalFlow=5
	
	Variable/G BC7_minConc_Thres=-100
	Variable/G r2_Thres=0.9
	Variable/G alpha_minThres=0.8
	Variable/G alpha_maxThres=3
	
	Variable/G minBCBool=1
	Variable/G r2Bool=1
	Variable/G alphaBool=1
	
	
	Make/O/N=7 cross_sec={18.47,14.54,13.14,11.58,10.35,7.77,7.19}
	Make/O/N=7 lambda={370,470,520,590,660,880,950}
	
	
	
End Function

Function SetValidationPanel(ctrlName)
	string ctrlName
	
	NewPanel/N=ValidationPanel/W=(0,0,200,200)/K=1

	Button RunVal_butt proc=RunVal, title="Run",win=ValidationPanel, size={60,30},pos={75,150}, fSize=18, fStyle=1

End Function


Function LoadAE33DataFiles(ctrlName) : ButtonControl
	string ctrlName

	SetDataFolder root:AE33data
	Variable refNum
	String message = "Select one or more files"
	String outputPaths
	String fileFilters = "All Files:.*;"
 
	Open /D /R /MULT=1 /F=fileFilters /M=message refNum
	outputPaths = S_fileName
 
	if (strlen(outputPaths) == 0)
		Print "Cancelled"

	else
		Variable numFilesSelected = ItemsInList(outputPaths, "\r")
		Variable i
		string nameofAE33file, nameOfBCdate
		for(i=0; i<numFilesSelected; i+=1)
			String path = StringFromList(i, outputPaths, "\r")

			LoadWave/Q/O/J/M/D/K=0/L={5,8,0,2,0}/V={" "," $",0,0} path
			wave wave0
			nameofAE33file="AE33_"+num2str(i)
			Rename wave0,$nameofAE33file
			
			LoadWave/A=BCtime_/Q/J/V={" "," $",0,0}/L={0,8,0,0,1}/R={French,2,2,2,1,"Year/Month/DayOfMonth",40} path
		endfor
		string ListofAE33Wave=Wavelist("AE33_*",";","")
		string ListofDateWave=Wavelist("BCtime_*",";","")
		Concatenate/O/NP=0 ListofDateWave, BCdate
		Concatenate/O/NP=0 ListofAE33Wave, Mx
		
		KillListofWaves(ListofAE33Wave)
		KillListofWaves(ListofDateWave)
		
	endif
	
	string/G header=""
	header+="Timebase;RefCh1;Sen1Ch1;Sen2Ch1;RefCh2;Sen1Ch2;Sen2Ch2;RefCh3;Sen1Ch3;Sen2Ch3;RefCh4;"
	header+="Sen1Ch4;Sen2Ch4;RefCh5;Sen1Ch5;Sen2Ch5;RefCh6;Sen1Ch6;Sen2Ch6;RefCh7;Sen1Ch7;Sen2Ch7;"
	header+="Flow1;Flow2;FlowC;Pressure;Temperature;BB;ContTemp;SupplyTemp;Status;ContStatus;DetectStatus;LedStatus;ValveStatus;LedTemp;"
	header+="BC11;BC12;BC1;BC21;BC22;BC2;BC31;BC32;BC3;BC41;BC42;BC4;BC51;BC52;BC5;BC61;BC62;BC6;BC71;BC72;BC7; "
	header+="K1;K2;K3;K4;K5;K6;K7;TapeAdvCount;"
	
	Display Mx[][38],Mx[][41],Mx[][44],Mx[][47],Mx[][50],Mx[][53], Mx[][56] vs BCdate
	ModifyGraph rgb(Mx)=(36864,14592,58880)
	ModifyGraph rgb(Mx#1)=(0,43520,65280)
	ModifyGraph rgb(Mx#2)=(0,65280,0)
	ModifyGraph rgb(Mx#3)=(58880,58880,0)
	ModifyGraph rgb(Mx#4)=(65280,0,0)
	ModifyGraph rgb(Mx#5)=(39168,13056,0)
	ModifyGraph rgb(Mx#6)=(0,0,0)
	ModifyGraph mode=2
	Label bottom " "
	Legend/C/N=text0/J/F=0/H={0,10,10}/A=MC "\\s(Mx) BC1\t\\s(Mx#1) BC2\t\\s(Mx#2) BC3\t\\s(Mx#3) BC4\t\\s(Mx#4) BC5\t\\s(Mx#5) BC6\t\\s(Mx#6) BC7"
	
	Make/O BC_CS={{52224,65280,0},{0,52420,0},{0,0,0}}
End Function


Function KillListofWaves(ListofWave)
	string ListOfWave
	variable i
	Variable numFilesSelected = ItemsInList(ListOfWave, ";")
	for(i=0; i<numFilesSelected; i+=1)
		String Wave2Kill = StringFromList(i, ListOfWave, ";")
		KillWaves/Z $Wave2Kill	
	endfor
End Function

Function PlotFLows(ctrlname)
	string ctrlName
	SetDataFolder root:AE33data
	wave BCdate, Mx
	Display Mx[][22],Mx[][23],Mx[][24] vs BCdate
	Label bottom " "
	ModifyGraph rgb(Mx)=(32768,65280,0)
	ModifyGraph rgb(Mx#2)=(34816,34816,34816)
	ModifyGraph rgb(Mx#1)=(16384,48896,65280)

End Function

Function EvaluateFlows(HighFlow, LowFlow, TotalFlow)
	variable HighFlow, LowFlow, TotalFlow
	
	SetDataFolder root:AE33data
	
	wave BCdate, Mx
	
	Make/O/N=(dimsize(Mx,0)) FlowBool
	FlowBool=1
	
	AppendToGraph/R FlowBool vs BCdate
	ReorderTraces Mx,{FlowBool}
	
	FlowBool[]=(Mx[p][22] > (Highflow*1.05) || Mx[p][22] < (Highflow*0.95)) ? 0 : FlowBool[p]
	FlowBool[]=(Mx[p][23] > (Lowflow*1.10) || Mx[p][23] < (Lowflow*0.9)) ? 0 : FlowBool[p]
	FlowBool[]=(Mx[p][24] > (Totalflow*1.05) || Mx[p][24] < (Totalflow*0.95)) ? 0 : FlowBool[p]
	
	print 100*sum(FlowBool)/numpnts(FlowBool)
End Function

//------------------------------------------------------------------------------------------------------------------------------------

Function ScanSpotChange(ctrlname)
	string ctrlname

	SetDataFolder root:AE33data
	wave BCdate, Mx
	Make/O/N=(dimsize(Mx,0)) SpotChangeBool,SpotChangeRatio, Status
	SpotChangeBool=0
	SpotChangeRatio=NaN
	Status[]=Mx[p][30]
	
	Duplicate/O Status Status_corr
	
	Status_corr[]=(Status[p]==0 || Status[p]==1 || Status[p]==2 || Status[p]==3) ? Status[p] : Status_corr[p]
	Status_corr[]=(Status[p]==4 || Status[p]==8 || Status[p]==12) ? 0 : Status_corr[p]
	Status_corr[]=(Status[p]==5 || Status[p]==9 || Status[p]==13) ? 1 : Status_corr[p]
	Status_corr[]=(Status[p]==6 || Status[p]==10 || Status[p]==14) ? 2 : Status_corr[p]
	Status_corr[]=(Status[p]==7) ? 3 : Status_corr[p]
	
	Status_corr[]=(Status[p]==17 || Status[p]==33 || Status[p]==49) ? 1 : Status_corr[p]
	Status_corr[]=(Status[p]==18 || Status[p]==34 || Status[p]==50) ? 2 : Status_corr[p]
	
	Status_corr[]=(Status[p]==128 || Status[p]==256) ? 0 : Status_corr[p]
	Status_corr[]=(Status[p]==129 || Status[p]==257 || Status[p]==385) ? 1 : Status_corr[p]
	Status_corr[]=(Status[p]==130 || Status[p]==258 || Status[p]==386) ? 2 : Status_corr[p]
	
	Status_corr[]=(Status[p]==387 || Status[p]==3331) ? 3 : Status_corr[p]
	
	SpotChangeBool[]=(Status_corr[p]==1 || Status_corr[p]==2) ? 1 : SpotChangeBool[p]
	
	variable i,j
	for (i=1;i<numpnts(BCdate);i+=1)
		//print i
//		if (Status[i]>256)
//			continue
//		endif
		
		if (Status_corr[i]==3)							//status = 3 means STOP. So need to find first next row with status = 0. No SC ratio to be calculated.
			FindLevel/Q/R=[i,numpnts(BCdate)-1] Status_corr, 0
			i=V_LevelX-1
			continue
		endif
		
		if (Mx[i][64]!=Mx[i-1][64])
			FindLevel/Q/R=[i,numpnts(BCdate)-1] Status_corr, Status_corr[i-1]
			j=V_LevelX
			SpotChangeRatio[i-1]=Mx[i-1][56]/Mx[j][56]
			i=j			
		endif
	endfor
	
	duplicate/O SpotChangeRatio SC_bool,SC_highlim, SC_lowlim
	SC_highlim=1.5
	SC_lowlim=0.5
	SC_bool=1
	SC_bool[]=(SpotChangeRatio[p]<SC_lowlim[p] || SpotChangeRatio[p]>SC_highlim[p]) ? 0 : SC_bool[p]
	
	
	Display SpotChangeRatio vs BCdate
	ModifyGraph mode=3,marker=19,useMrkStrokeRGB=1
	Label bottom " "
	ModifyGraph zmrkSize(SpotChangeRatio)={Mx[*][56],0,*,1,10}
	AppendToGraph SC_lowlim,SC_highlim vs BCdate
	ModifyGraph rgb(SC_highlim)=(56576,56576,56576),rgb(SC_lowlim)=(56576,56576,56576)
	ReorderTraces SpotChangeRatio,{SC_highlim,SC_lowlim}
	ModifyGraph mode(SC_highlim)=7,toMode(SC_highlim)=1,hbFill(SC_highlim)=2
	ModifyGraph zColor(SpotChangeRatio)={SC_bool,*,*,EOSSpectral11,0}
	
	if (wavemin(SpotChangeRatio)>=0)
		SetAxis left 0,*
	endif
	
	//print 100*sum(SC_bool)/numpnts(SC_bool)
	
	//KillWaves Status
	
	SC_Histogram(SpotChangeRatio)
	
	Make/O/N=(numpnts(BCdate)) color
	color=2
	for (i=0;i<numpnts(BCdate);i+=1)
		if (SC_bool[i]==0)
			FindLevel/Q/R=[i,0] Status_corr, 2
			color[V_levelX+1,i]=1
		endif
	endfor
	
	
End Function

//------------------------------------------------------------------------------------------------------------------------------------------------

Function ValidationAE33_v2(R2criteria,LowAlpha,HighAlpha, MinBC)
	variable R2criteria,LowAlpha,HighAlpha, MinBC
	
	SetDataFolder root:AE33data
	wave BCdate, Mx, Status_corr, cross_sec,lambda
	
	Make/O/N=7 tempYwave
	variable rows=dimsize(Mx,0)
	Make/O/N=(rows,7) BC_mx
	Make/O/N=(rows) Angstrom, r2, BC1, BC2, BC3, BC4, BC5, BC6, BC7
	
	BC1[]=Mx[p][38]
	BC2[]=Mx[p][41]
	BC3[]=Mx[p][44]
	BC4[]=Mx[p][47]
	BC5[]=Mx[p][50]
	BC6[]=Mx[p][53]
	BC7[]=Mx[p][56]
	
	duplicate/O BC1 BC1_corr
	duplicate/O BC2 BC2_corr
	duplicate/O BC3 BC3_corr
	duplicate/O BC4 BC4_corr
	duplicate/O BC5 BC5_corr
	duplicate/O BC6 BC6_corr
	duplicate/O BC7 BC7_corr
	
	BC_mx[][0]=Mx[p][38]
	BC_mx[][1]=Mx[p][41]
	BC_mx[][2]=Mx[p][44]
	BC_mx[][3]=Mx[p][47]
	BC_mx[][4]=Mx[p][50]
	BC_mx[][5]=Mx[p][53]
	BC_mx[][6]=Mx[p][56]
	
	Make/O/N=(rows) r2BoolWave=1,AlphaBoolWave=1,NegBoolWave=1,MeasureData=1
	variable i,j

	NegBoolWave[]=(BC_mx[p][0]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC_mx[p][1]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC_mx[p][2]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC_mx[p][3]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC_mx[p][4]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC_mx[p][5]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC_mx[p][6]<-100)? 0 : NegBoolWave[p]

	duplicate/O BC_mx Babs_mx
	
	for (i=0;i<7;i+=1)
		Babs_mx[][i]*=cross_sec[i]/1000000
	endfor
	
	MatrixOP/O LN_mx=ln(Babs_mx)
	MatrixOP/O LN_lambda=ln(lambda/1000000000)
	
	MatrixTranspose LN_mx
	wave LN_mx, LN_lambda
	variable OK=0,alphaNotOK=0,r2NotOK=0,neg
	
	neg=numpnts(NegBoolWave)-sum(NegBoolWave)
	
	for (i=0;i<rows;i+=1)
		if (NegBoolWave[i]==0 || BC_mx[i][0]<-100 || BC_mx[i][1]<0 || BC_mx[i][2]<0 || BC_mx[i][3]<0 || BC_mx[i][4]<0 || BC_mx[i][5]<0 || BC_mx[i][6]<0)
			continue
		endif
		
		if (Status_corr[i] !=0)
			//MeasureData[i]=0
			continue
		endif
		
		tempYwave[]=LN_mx[p][i]
		
		CurveFit/Q/NTHR=0 line tempYwave/X=LN_lambda/D
		wave W_coef
		Angstrom[i]=-W_coef[1]
		r2[i]=V_r2
//		if (Angstrom[i]<LowAlpha || Angstrom[i]>HighAlpha || BC7[i]<MinBC)
		if (BC7[i]>MinBC && (Angstrom[i]<LowAlpha || Angstrom[i]>HighAlpha))
			AlphaBoolWave[i]=0
			alphaNotOK+=1
			//continue
		endif
//		if (numtype(r2[i])==2 || r2[i]<R2criteria  || BC7[i]<MinBC)
		if (BC7[i]>MinBC && (numtype(r2[i])==2 || r2[i]<R2criteria))
			r2NotOK+=1
			r2BoolWave[i]=0
			//continue
		endif
//		if (Angstrom[i]<LowAlpha || Angstrom[i]>HighAlpha)
//			AlphaBoolWave[i]=0
//			alphaNotOK+=1
//			continue
//		endif
	endfor
	
	Make/O/N=(rows) BoolWave=r2BoolWave*AlphaBoolWave*NegBoolWave*MeasureData
	
	//r2[]=(BoolWave[p]==0)? NaN : r2[p]
	//Angstrom[]=(BoolWave[p]==0)? NaN : Angstrom[p]

	BC1_corr[]=(BoolWave[p]==0)? NaN : BC1_corr[p]
	BC2_corr[]=(BoolWave[p]==0)? NaN : BC2_corr[p]
	BC3_corr[]=(BoolWave[p]==0)? NaN : BC3_corr[p]
	BC4_corr[]=(BoolWave[p]==0)? NaN : BC4_corr[p]
	BC5_corr[]=(BoolWave[p]==0)? NaN : BC5_corr[p]
	BC6_corr[]=(BoolWave[p]==0)? NaN : BC6_corr[p]
	BC7_corr[]=(BoolWave[p]==0)? NaN : BC7_corr[p]
	
	wave color
	color[]=(BoolWave[p]==0)? 0 : color[p]
	
	Display Angstrom vs BC7_corr
	ModifyGraph mode=2,lsize=2,zColor(Angstrom)={BCdate,*,*,Rainbow,1}
	Label bottom "BC7_corr"
	Label left "Angstrom"
	ColorScale/C/N=text0/F=0/A=MC trace=Angstrom
	ColorScale/C/N=text0 " "
	
	wave fit_tempYwave,W_sigma
	
	//KillWaves BoolWave,cross_sec,lambda,tempYwave,W_coef,fit_tempYwave,W_sigma,BC_mx,Babs_mx,LN_mx,LN_lambda,BC_mx
	
	Extract/O BC7,BC7_badr,r2BoolWave==0
	Make/N=100/O BC7_badr_Hist
	Histogram/B={-100,50,100} BC7_badr,BC7_badr_Hist
	Display BC7_badr_Hist
	ModifyGraph mode=5,hbFill=2,useBarStrokeRGB=1
	Label left "count"
	Label bottom "BC7_corr"
	TextBox/C/N=text0/F=0/A=MC "Bad r�"
	
	Extract/O BC7,BC7_bada,AlphaBoolWave==0
	Make/N=100/O BC7_bada_Hist
	Histogram/B={-100,50,100} BC7_bada,BC7_bada_Hist
	Display BC7_bada_Hist
	ModifyGraph mode=5,hbFill=2,useBarStrokeRGB=1
	Label left "count"
	Label bottom "BC7_corr"
	TextBox/C/N=text0/F=0/A=MC "Bad Angstrom exponent"
	
	KillWaves BC7_bada, BC7_badr,NegBoolWave
	
	Display BC7 vs BCdate
	wave BC_CS
	ModifyGraph zColor(BC7)={color,*,*,cindexRGB,0,BC_CS}
	Legend/C/N=text0/J/F=0/H={0,10,10}/A=MC "\\K(52224,0,0)\\W516 Invalid Data\r\r\\K(65280,43520,0)\\W516 Possible Loss of Compensation\r\r\\K(0,0,0)\\W516 Valid Data"
	Label bottom " "
	Label left "Concentration (ng/m3)"
	
	
	
End Function


Function Validation_AE33(time_wave,BC1,BC2,BC3,BC4,BC5,BC6,BC7, R2criteria,LowAlpha,HighAlpha,minBC)
	
	wave time_wave,BC1,BC2,BC3,BC4,BC5,BC6,BC7
	variable R2criteria,LowAlpha,HighAlpha,minBC
	Make/O/N=7 cross_sec={18.47,14.54,13.14,11.58,10.35,7.77,7.19}
	Make/O/N=7 lambda={370,470,520,590,660,880,950}
	Make/O/N=7 tempYwave
	variable rows=numpnts(BC1)
	Make/O/N=(rows,7) BC_mx
	Make/O/N=(rows) Angstrom, r2
	BC_mx[][0]=BC1[p]
	BC_mx[][1]=BC2[p]
	BC_mx[][2]=BC3[p]
	BC_mx[][3]=BC4[p]
	BC_mx[][4]=BC5[p]
	BC_mx[][5]=BC6[p]
	BC_mx[][6]=BC7[p]
	
	Make/O/N=(rows) r2BoolWave=1,AlphaBoolWave=1,SpotBoolWave=1,NegBoolWave=1,SC=0, SC_BC=0, SC_1L=1
	variable i,j

	NegBoolWave[]=(BC1[p]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC2[p]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC3[p]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC4[p]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC5[p]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC6[p]<-100)? 0 : NegBoolWave[p]
	NegBoolWave[]=(BC7[p]<-100)? 0 : NegBoolWave[p]
	SpotBoolWave[]=(BC1[p]==0)? 0 : SpotBoolWave[p]
	SpotBoolWave[]=(BC2[p]==0)? 0 : SpotBoolWave[p]
	SpotBoolWave[]=(BC3[p]==0)? 0 : SpotBoolWave[p]
	SpotBoolWave[]=(BC4[p]==0)? 0 : SpotBoolWave[p]
	SpotBoolWave[]=(BC5[p]==0)? 0 : SpotBoolWave[p]
	SpotBoolWave[]=(BC6[p]==0)? 0 : SpotBoolWave[p]
	SpotBoolWave[]=(BC7[p]==0)? 0 : SpotBoolWave[p]
	
	duplicate/O BC_mx Babs_mx
	
	for (i=0;i<7;i+=1)
		Babs_mx[][i]*=cross_sec[i]/1000000
	endfor
	
	MatrixOP/O LN_mx=ln(Babs_mx)
	MatrixOP/O LN_lambda=ln(lambda/1000000000)
	
	MatrixTranspose LN_mx

	variable OK=0,alphaNotOK=0,r2NotOK=0,spot,neg
	
	spot=numpnts(SpotBoolWave)-sum(SpotBoolWave)
	neg=numpnts(NegBoolWave)-sum(NegBoolWave)
	
	for (i=0;i<rows;i+=1)
		if (NegBoolWave[i]==0)
			continue
		endif
		if (SpotBoolWave[i]==0)
			//continue
			if (SpotBoolWave[i+5]==0)
				//SC[i-1]=(BC7[i+6]/BC7[i-1])
				SC[i-1]=(BC7[i-1]/BC7[i+6])
				
				SC_BC[i-1]=BC7[i-1]
				i+=6
				continue
			else
				//SC[i-1]=(BC7[i+5]/BC7[i-1])
				SC[i-1]=(BC7[i-1]/BC7[i+5])
				SC_BC[i-1]=BC7[i-1]
				i+=5
				continue
			endif
		endif
		tempYwave[]=LN_mx[p][i]
		
		CurveFit/Q/NTHR=0 line tempYwave/X=LN_lambda/D
		wave W_coef
		Angstrom[i]=-W_coef[1]
		r2[i]=V_r2
//		if (Angstrom[i]<LowAlpha || Angstrom[i]>HighAlpha || BC7[i]<MinBC)
		if (BC7[i]>MinBC && (Angstrom[i]<LowAlpha || Angstrom[i]>HighAlpha))
			AlphaBoolWave[i]=0
			alphaNotOK+=1
			//continue
		endif
//		if (numtype(r2[i])==2 || r2[i]<R2criteria  || BC7[i]<MinBC)
		if (BC7[i]>MinBC && (numtype(r2[i])==2 || r2[i]<R2criteria))
			r2NotOK+=1
			r2BoolWave[i]=0
			//continue
		endif
//		if (Angstrom[i]<LowAlpha || Angstrom[i]>HighAlpha)
//			AlphaBoolWave[i]=0
//			alphaNotOK+=1
//			continue
//		endif
	endfor
	
	Make/O/N=(rows) BoolWave=r2BoolWave*AlphaBoolWave*SpotBoolWave*NegBoolWave
	
	//r2[]=(BoolWave[p]==0)? NaN : r2[p]
	//Angstrom[]=(BoolWave[p]==0)? NaN : Angstrom[p]
	duplicate/O BC1, BC1_corr
	duplicate/O BC2, BC2_corr
	duplicate/O BC3, BC3_corr
	duplicate/O BC4, BC4_corr
	duplicate/O BC5, BC5_corr
	duplicate/O BC6, BC6_corr
	duplicate/O BC7, BC7_corr
	BC1_corr[]=(BoolWave[p]==0)? NaN : BC1_corr[p]
	BC2_corr[]=(BoolWave[p]==0)? NaN : BC2_corr[p]
	BC3_corr[]=(BoolWave[p]==0)? NaN : BC3_corr[p]
	BC4_corr[]=(BoolWave[p]==0)? NaN : BC4_corr[p]
	BC5_corr[]=(BoolWave[p]==0)? NaN : BC5_corr[p]
	BC6_corr[]=(BoolWave[p]==0)? NaN : BC6_corr[p]
	BC7_corr[]=(BoolWave[p]==0)? NaN : BC7_corr[p]
	
	SC[]=(SC[p]==0)? NaN : SC[p]
	SC_BC[]=(SC_BC[p]==0)? NaN : SC_BC[p]
	
	Display Angstrom vs BC7_corr
	ModifyGraph mode=2,lsize=2,zColor(Angstrom)={time_wave,*,*,Rainbow,1}
	Label bottom "BC7_corr"
	Label left "Angstrom"
	ColorScale/C/N=text0/F=0/A=MC trace=Angstrom
	ColorScale/C/N=text0 " "
	
	wave fit_tempYwave,W_sigma
	
	KillWaves BoolWave,cross_sec,lambda,tempYwave,W_coef,fit_tempYwave,W_sigma,BC_mx,Babs_mx,LN_mx,LN_lambda,BC_mx
	
	print "number of rows relative to spot changes:"
	print spot
	print "% of rows containing negative values:"
	print 100*neg/(rows-spot)
	print "% of bad r�:"
	print (100*(r2NotOK)/(rows-spot))
	print "% of bad alpha:"
	print (100*alphaNotOK/(rows-spot))
	print "% of valid data points:"
	print (100*(rows-spot-r2NotOK-alphaNotOK-neg)/(rows-spot))
	
	Extract/O BC7,BC7_badr,r2BoolWave==0
	Make/N=100/O BC7_badr_Hist
	Histogram/B={-100,50,100} BC7_badr,BC7_badr_Hist
	Display BC7_badr_Hist
	ModifyGraph mode=5,hbFill=2,useBarStrokeRGB=1
	Label left "count"
	Label bottom "BC7_corr"
	TextBox/C/N=text0/F=0/A=MC "Bad r�"
	
	Extract/O BC7,BC7_bada,AlphaBoolWave==0
	Make/N=100/O BC7_bada_Hist
	Histogram/B={-100,50,100} BC7_bada,BC7_bada_Hist
	Display BC7_bada_Hist
	ModifyGraph mode=5,hbFill=2,useBarStrokeRGB=1
	Label left "count"
	Label bottom "BC7_corr"
	TextBox/C/N=text0/F=0/A=MC "Bad Angstrom exponent"
	
	Display SC vs time_wave
	ModifyGraph mode=4,marker=19,zmrkSize(SC)={SC_BC,*,*,1,20}
	ModifyGraph useMrkStrokeRGB(SC)=1
	ModifyGraph rgb(SC)=(0,39168,39168)
	ModifyGraph axRGB(left)=(0,39168,39168),tlblRGB(left)=(0,39168,39168);DelayUpdate
	ModifyGraph alblRGB(left)=(0,39168,39168)
	SetAxis left 0,*
	Label left "Spot change ratio"
	TextBox/C/N=text0/F=0/A=MC "size of marker dependent of BC7 at spot change"
	AppendToGraph SC_1L vs time_wave
	ModifyGraph lstyle(SC_1L)=3,rgb(SC_1L)=(39168,39168,39168)
	ReorderTraces SC,{SC_1L}
	ModifyGraph axisEnab(bottom)={0,0.6}
	AppendToGraph/R=R1 BC7_corr vs time_wave
	ModifyGraph freePos(R1)={0.41,kwFraction}
	ModifyGraph mode(BC7_corr)=2,rgb(BC7_corr)=(52224,52224,52224)
	ReorderTraces SC_1L,{BC7_corr}
	AppendToGraph/L=L1/B=B1 SC vs SC_BC
	ModifyGraph axisEnab(B1)={0.7,1},freePos(L1)={0,B1},freePos(B1)={0,L1}
	ModifyGraph mode(SC#1)=3,marker(SC#1)=19,rgb(SC#1)=(0,39168,39168)
	SetAxis L1 0,*
	ModifyGraph useMrkStrokeRGB(SC#1)=1
	
	
	
	
	KillWaves BC7_bada, BC7_badr,NegBoolWave,SpotBoolWave
	
	SC_Histogram(SC)
	
	
End Function

//-------------------------------------------------------------------------------------------------------------------------------------------

Function SC_Histogram(SC)
	wave SC
	Make/N=100/O SC_Hist;DelayUpdate
	Histogram/B={-5,0.1,100} SC,SC_Hist;DelayUpdate
	Display SC_Hist
	ModifyGraph mode=5,hbFill=2,useBarStrokeRGB=1
	ModifyGraph nticks(bottom)=10
	CurveFit/M=2/W=0 gauss, SC_Hist/D
	wave W_coef
	TextBox/C/N=text0/F=0/H={0,10,10}/A=MC ("x\B0\M = " + num2str(W_coef[2]) + "\r median= " + num2str(AE33_Median(SC)))
	ModifyGraph lstyle(fit_SC_Hist)=3,lsize(fit_SC_Hist)=2,rgb(fit_SC_Hist)=(0,0,0)

End Function

//--------------------------------------------------------------------------------------------------------------------------

Function Sandradewi(L_UV,L_IR,BC_UV,BC_IR,BC,a_wb,a_ff)
	wave BC_UV, BC_IR, BC
	variable a_wb, a_ff,L_UV,l_IR
	
	variable LO_ratio=L_UV/L_IR
	
	variable LO_ratio_wb=LO_ratio^a_wb
	variable LO_ratio_ff=LO_ratio^(-a_ff)
	
	Make/O/N=(numpnts(BC)) babs_UV,babs_IR,babs_wb_UV,babs_wb_IR,BB,BCwb,BCff
	babs_UV=BC_UV*14.54
	babs_IR=BC_IR*7.19
	
	babs_wb_UV=(babs_UV-LO_ratio_ff*babs_IR)/(1-LO_ratio_wb*LO_ratio_ff)
	babs_wb_IR=babs_wb_UV*LO_ratio_wb
	
	//BB=min(max(0,babs_wb_IR/babs_IR),1)
	BB=babs_wb_IR/babs_IR
	
	BCwb=BB*BC
	BCff=BC-BCwb
	
	//BCwb[] = (BB[p]==1) ? NaN : BCwb[p]
	//BCff[] = (BB[p]==1) ? NaN : BCff[p]
	
	string nameWB="BCwb_aff"+num2str(a_ff)+"_awb"+num2str(a_wb)
	string nameFF="BCff_aff"+num2str(a_ff)+"_awb"+num2str(a_wb)
	
	duplicate/O BCwb $nameWB 
	duplicate/O BCff $nameFF

	KillWaves babs_UV,babs_IR,babs_wb_UV,babs_wb_IR,BCwb,BCff

End Function

//-------------------------------------------------------------------------------------------------------------------------------------------

Function hour_BP(dt)
	Variable dt					// Input date/time value
	Variable time = mod(dt, 24*60*60)	// Get the time component of the date/time
	return trunc(time/(60*60))
End

//-------------------------------------------------------------------------------------------------------------------------------------------

Function Sandradewi2(BC_UV,BC_IR,BC,a_wb,a_ff)
	wave BC_UV, BC_IR, BC
	variable a_wb, a_ff
	
	variable LO_UV=470,LO_IR=950
	
	variable LO_ratio=LO_UV/LO_IR
	variable LO_ratio_wb
	variable LO_ratio_ff
	string nameWB
	string nameFF
	
	variable i
	
	for (i=a_wb-0.05;i<=a_wb+0.05;i+=0.01)
		LO_ratio_wb=LO_ratio^i
		LO_ratio_ff=LO_ratio^(-a_ff)
	
	
	
		Make/O/N=(numpnts(BC)) babs_UV,babs_IR,babs_wb_UV,babs_wb_IR,BB,BCwb,BCff
		babs_UV=BC_UV*14.54
		babs_IR=BC_IR*7.19
	
		babs_wb_UV=(babs_UV-LO_ratio_ff*babs_IR)/(1-LO_ratio_wb*LO_ratio_ff)
		babs_wb_IR=babs_wb_UV*LO_ratio_wb
	
		BB=min(max(0,babs_wb_IR/babs_IR),1)
	
		BCwb=BB*BC
		BCff=BC-BCwb
	
		nameWB="BCwb_aff"+num2str(a_ff)+"_awb"+num2str(i)
		nameFF="BCff_aff"+num2str(a_ff)+"_awb"+num2str(i)
	
		duplicate/O BCwb $nameWB 
		duplicate/O BCff $nameFF

		KillWaves babs_UV,babs_IR,babs_wb_UV,babs_wb_IR,BCwb,BCff
	endfor
	
	for (i=a_ff-0.05;i<=a_ff+0.05;i+=0.01)
		LO_ratio_wb=LO_ratio^a_wb
		LO_ratio_ff=LO_ratio^(-i)
	
	
	
		Make/O/N=(numpnts(BC)) babs_UV,babs_IR,babs_wb_UV,babs_wb_IR,BB,BCwb,BCff
		babs_UV=BC_UV*14.54
		babs_IR=BC_IR*7.19
	
		babs_wb_UV=(babs_UV-LO_ratio_ff*babs_IR)/(1-LO_ratio_wb*LO_ratio_ff)
		babs_wb_IR=babs_wb_UV*LO_ratio_wb
	
		BB=min(max(0,babs_wb_IR/babs_IR),1)
	
		BCwb=BB*BC
		BCff=BC-BCwb
	
		nameWB="BCwb_aff"+num2str(i)+"_awb"+num2str(a_wb)
		nameFF="BCff_aff"+num2str(i)+"_awb"+num2str(a_wb)
	
		duplicate/O BCwb $nameWB 
		duplicate/O BCff $nameFF

		KillWaves babs_UV,babs_IR,babs_wb_UV,babs_wb_IR,BCwb,BCff
	endfor
	
End Function

//-------------------------------------------------------------------------------------------------------------

Function Sandradewi3(L_UV,L_IR,BC_UV,BC_IR,BC)
	wave BC_UV, BC_IR, BC
	variable L_UV,l_IR
	
	variable LO_ratio=L_UV/L_IR
	
	Make/O/N=21 a_wb_range, a_ff_range=1, nb_neg=0
	a_wb_range[]=1.5+(p*0.05)
	
	variable i,a_wb,a_ff,LO_ratio_wb, LO_ratio_ff
	
	for (i=0;i<numpnts(a_wb_range);i+=1)
		a_wb=a_wb_range[i]
		a_ff=a_ff_range[i]
		LO_ratio_wb=LO_ratio^a_wb
		LO_ratio_ff=LO_ratio^(-a_ff)
		
		Make/O/N=(numpnts(BC)) babs_UV,babs_IR,babs_wb_UV,babs_wb_IR,BB,BCwb,BCff
		babs_UV=BC_UV*14.54
		babs_IR=BC_IR*7.19
		
		babs_wb_UV=(babs_UV-LO_ratio_ff*babs_IR)/(1-LO_ratio_wb*LO_ratio_ff)
		babs_wb_IR=babs_wb_UV*LO_ratio_wb
		
		BB=babs_wb_IR/babs_IR
		
		BCwb=BB*BC
		BCff=BC-BCwb
		
		Extract/O BCff, temp, (BCff<0)
		
		nb_neg[i]=numpnts(temp)
		
	endfor
End Function

//----------------------------------------------------------------------------------------------------------

Function/D AE33_Median(w) // Returns median value of wave w
	Wave w
	
	Variable result
	
	Duplicate/O w, tempMedianWave // Make a clone of wave
	WaveTransform zapNaNs tempMedianWave
	Sort tempMedianWave, tempMedianWave // Sort clone
	SetScale/P x 0,1,tempMedianWave
	result = tempMedianWave((numpnts(tempMedianWave)-1)/2)
	KillWaves tempMedianWave
	
	return result
End

//--------------------------------------------------------------------------------------------------------------------------------------------------------

Function Angstrom_calc(L_UV,L_IR)

	variable L_UV,L_IR
	wave LN_mx,LN_lambda, BC7_corr, lambda, cross_sec
	
	variable numberUV, numberIR
	
	FindLevel lambda, L_UV 
	numberUV=V_levelX
	
	FindLevel lambda, L_IR
	numberIR=V_levelX
	
	
	variable sigmaUV2use=cross_sec[numberUV]
	variable sigmaIR2use=cross_sec[numberIR]
	
	string BCUVname, BCIRname
	BCUVname="BC"+num2str(numberUV+1)+"_corr"
	BCIRname="BC"+num2str(numberIR+1)+"_corr"
	
	wave BC_UV_2use=$BCUVname
	wave BC_IR_2use=$BCIRname
	
	String Angstrom_name="Angstrom_"+num2str(L_UV)+"_"+num2str(L_IR)
	
	Duplicate/O BC_IR_2use $Angstrom_name
	
	Wave New_Angstrom= $Angstrom_name
	
	variable i
	for (i=0;i<numpnts(BC_IR_2use);i+=1)
		New_Angstrom[i]=-(ln(BC_UV_2use[i]*sigmaUV2use/1000000)-ln(BC_IR_2use[i]*sigmaIR2use/1000000))/(ln(L_UV)-ln(L_IR))
	endfor
	
	New_Angstrom[]=(BC_IR_2use[p]<200) ? NaN : New_Angstrom[p]
	
	//fWavePercentile(NameOfWave(New_Angstrom), "5; 10; 25; 50; 75; 90; 95", NameOfWave(New_Angstrom)+"_p", 0, 	0, 0)
	
	Make/N=100/O Angstrom_hist
	Histogram/B={0.5,0.025,100} New_Angstrom,Angstrom_hist
	Display Angstrom_hist
	ModifyGraph mode=5,hbFill=2,useBarStrokeRGB=1
	ModifyGraph nticks(bottom)=10
	CurveFit/M=2/W=0 gauss, Angstrom_hist/D
	wave W_coef
//	TextBox/C/N=text0/F=0/H={0,10,10}/A=MC ("x\B0\M = " + num2str(W_coef[2]) + "\r median= " + num2str(AE33_Median(New_Angstrom)))
	ModifyGraph lstyle(fit_Angstrom_hist)=3,lsize(fit_Angstrom_hist)=2,rgb(fit_Angstrom_hist)=(0,0,0)
	
End Function


Menu "GraphMarquee"
	"AE33: Manual Invalidation from Marquee" , AE33_Manual_Invalidation()
End


Function AE33_Manual_Invalidation()
	wave color, BC1, BC2, BC3, BC4, BC5, BC6, BC7, BCdate
	GetMarquee left, bottom
	if (V_flag == 0)
		Print "There is no marquee"
	else
		color[BinarySearch(BCdate,V_left), BinarySearch(BCdate,V_right)]=0
		duplicate/O BC1 BC1_corr
		duplicate/O BC2 BC2_corr
		duplicate/O BC3 BC3_corr	
		duplicate/O BC4 BC4_corr
		duplicate/O BC5 BC5_corr
		duplicate/O BC6 BC6_corr
		duplicate/O BC7 BC7_corr
	
		BC1_corr[]=(color[p]==0)? NaN : BC1_corr[p]
		BC2_corr[]=(color[p]==0)? NaN : BC2_corr[p]
		BC3_corr[]=(color[p]==0)? NaN : BC3_corr[p]
		BC4_corr[]=(color[p]==0)? NaN : BC4_corr[p]
		BC5_corr[]=(color[p]==0)? NaN : BC5_corr[p]
		BC6_corr[]=(color[p]==0)? NaN : BC6_corr[p]
		BC7_corr[]=(color[p]==0)? NaN : BC7_corr[p]
	endif
End Function


Function GenerateFlagWaves()
	wave BCdate, color
	variable i, j
	Make/O/N=0 FlagWave
	Duplicate/O BCdate startwave
	Duplicate/O BCdate endwave
	DeletePoints 0,numpnts(BCdate), startWave
	DeletePoints 0,numpnts(BCdate), endWave
	
	for(i=0;i<numpnts(color);i+=1)
		if(color[i]==0)
			for(j=i+1;j<numpnts(color);j+=1)
				if(color[j]!=0)
					break
				endif
			endfor
		InsertPoints Numpnts(startWave),1, startWave, EndWave, FlagWave
		startWave[Numpnts(startWave)-1]=BCdate[i]
		EndWave[Numpnts(startWave)-1]=BCdate[j-1]
		FlagWave[Numpnts(startWave)-1]=-999
		i=j
		endif
	
	endfor
	

End Function